package com.example.xerks.kotlinhw2

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_final.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val CAMERA_REQUEST = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var goToCameraBtn: Button = findViewById(R.id.goToCameraBtn)


        goToCameraBtn.setOnClickListener(View.OnClickListener {
            if (editName.getText().toString().trim().length == 0) {
                Toast.makeText(applicationContext, "Wrong name", Toast.LENGTH_SHORT).show()
            } else {
                /*var intent = Intent(applicationContext, MainActivity::class.java)
                intent.setType("text/plain")
                intent.putExtra(Intent.EXTRA_TEXT, text)*/
                val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (callCameraIntent.resolveActivity(packageManager) != null) {
                    startActivityForResult(callCameraIntent, CAMERA_REQUEST)

                    //intent.putExtra(Intent. ))
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CAMERA_REQUEST -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    setContentView(R.layout.activity_final)
                    photoImageView.setImageBitmap(data.extras.get("data") as Bitmap)


                }
            }
            else -> {
                Toast.makeText(this, "что-то не так", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
